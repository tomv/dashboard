// Add your javascript here
var animation_speed = 1000;
$(function() {

  var progressBar = $('#pb1');
  var progStatus = $('#progVal');
  var progBarLabel = $('#progLabel');
  progressBar.click(function(e) {
    alert("you clicked the progress bar!");
  })
  setInterval(function() {
    updateProgress(progStatus, progressBar)
  }, animation_speed);


});

//Functiion to update progress bar

var tempValues = [93,97,109,91,94];
var pressureValues = [0, 0, 0, 0, 1, 2, 5, 4, 7, 9, 10, 15, 20, 50, 90, 95, 96, 98, 100, 100, 100, 100];
var rpmValues = [200, 250,230, 235, 310];

 progValues = [  76, 72, 73, 70, 71, 75 ];

var progValIndex = 0;

function updateProgress(status, bar) {
  var progVal;
  var progValuesCount = progValues.length;

  // DEGUG console.log("Index: %s, Items: %s", progValIndex, progValuesCount);
  if (progValIndex + 1 == progValuesCount) {
    // We are done.. 
    progValIndex = 0;
  } else {
    progValIndex++;
  }
  progVal = progValues[progValIndex];
  status.html(progVal);
  //  bar.attr('value', progVal);

  bar.animate({
    'width': progVal + '%',
    'value': progVal
  }, (animation_speed*0.75))
  // console.log("Updated bar:  " + progVal);
}